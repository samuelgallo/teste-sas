# Teste de desenvolvimento para smartagesolutions

Proposta, desenvolver um sistema de SAC resumido onde possa ser registrado a origem do chamado (telefone, chat, email), estado, motivo do chamado e assunto.

Caso tenha alguma dúvida bastar enviar um email para samuel@samuelgallo.com

#### Execução local

Você deve ter o [Node.js](http://nodejs.org/), [NPM](https://www.npmjs.com/get-npm) e o [MySQL](https://dev.mysql.com/downloads/mysql/) instalados.

```sh
$ git clone git@bitbucket.org:samuelgallo/teste-sas.git
$ cd teste-sas
$ npm install
$ node index.js
```

A aplicação agora deve estar rodando em [localhost:3000](http://localhost:3000/).

#### Node.js
A escolha do Node.js se deu pela versatilidade por ser capaz de entregar o backend e o frontend ao mesmo tempo, pela facilidade de manutenção e desenvolvimento, possibilidade de utilização de um framework voltado somente para frontend, escalabilidade.

#### Gulp
O Gulp foi utilizado neste teste para minificar o css e o js como exemplo, pois em um projeto complexo poderíamos utilizar todas as vantagens do Gulp como o browser-sync para atualizar o brower a cada alteração realizada, imagemin para comprimir todas as images do projeto, gulp-concat para concatenar os arquivos e o gulp-uglify para comprimir o js e verificar possíveis erros.


#### Mysql
O Mysql foi escolhido por ser um dos bancos de dados mais utilizados quando o backend e o php.

Alterando a conexão com o banco de dados
/config/dbConnection.js
```
host : 'localhost',
		user : 'SeuUsuário',
		password : 'SuaSenha',
		database : 'SeuBancoDeDados'
```

Criando o banco de dados
```
$ create database sac;
```

Criando a tabela
```
create table atendimento(
    id_atendimento int not null primary key auto_increment,
    origem varchar(50),
    assunto varchar(50),
    estado varchar(50),
    cliente varchar(100),
    atendente varchar(100),    
    mensagem text,
    data_criacao timestamp default current_timestamp
);
```

Populando o Mysql
```
insert into atendimento (id_atendimento, origem, assunto, estado, cliente, atendente, mensagem, data_criacao) values  (default, 'Chat', 'Dúvida', 'RJ', 'João Silva', 'Sandra', 'Bacon ipsum dolor amet meatloaf prosciutto sirloin, strip steak beef tenderloin burgdoggen doner porchetta tongue boudin. Ham hock bresaola venison pancetta sausage turkey ribeye filet mignon picanha fatback jowl pork chop bacon corned beef.', now());
```

```
insert into atendimento (id_atendimento, origem, assunto, estado, cliente, atendente, mensagem, data_criacao) values  (default, 'Telefone', 'Crítica', 'SP', 'Maria Silva', 'Sandra', 'Bacon ipsum dolor amet meatloaf prosciutto sirloin, strip steak beef tenderloin burgdoggen doner porchetta tongue boudin. Ham hock bresaola venison pancetta sausage turkey ribeye filet mignon picanha fatback jowl pork chop bacon corned beef.', now());
```


##### Itens que melhorariam este projeto
TDD - mocha ([Exemplo](https://semaphoreci.com/community/tutorials/,a-tdd-approach-to-building-a-todo-api-using-node-js-and-mongodb))
NoSQL - MongoDB,
Url - amigável,
[Material Design Lite](https://getmdl.io/) - Alternativa ao Bootstrap,
[SOCKET.IO](https://socket.io/) - Caso existissem múltiplos usuários na plataforma


