module.exports.adicionar = function(application, req, res){
    res.render('admin/form_add', { validacao : {}, atendimento : {} });
}

module.exports.sac_salvar = function(application, req, res){
    var atendimento = req.body;

    req.assert('origem', 'A origem do atendimento é obrigatória').notEmpty();
    req.assert('assunto', 'O assunto é obrigatório').notEmpty();
    req.assert('estado', 'O estado é obrigatório').notEmpty();
    req.assert('cliente', 'Nome do cliente é obrigatório').notEmpty();
    req.assert('atendente', 'Nome do atendente é obrigatório').notEmpty();    
    
    req.assert('mensagem', 'Mensagem é obrigatória').notEmpty();

    var erros = req.validationErrors();

    if(erros){
        res.render('admin/form_add', {validacao: erros,  atendimento: atendimento});
        return;
    }   

    var connection = application.config.dbConnection();
    var sacModel = new application.app.models.SacDAO(connection);

    sacModel.salvarAtendimento(atendimento, function(error, result){
        res.redirect('/sac');
    });
}