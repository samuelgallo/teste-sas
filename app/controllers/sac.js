module.exports.sac = function(application, req, res){
    var connection = application.config.dbConnection();
    var sacModel = new application.app.models.SacDAO(connection);

    sacModel.getSac(function(error, result){
        res.render('sac/sac', { sac : result });
    });
}

module.exports.atendimento = function(application, req, res){
    var connection = application.config.dbConnection();
    var sacModel = new application.app.models.SacDAO(connection);

    var id_atendimento = req.query;

    sacModel.getAtendimento(id_atendimento, function(error, result){
        res.render('sac/atendimento', { atendimento : result });
    });
}