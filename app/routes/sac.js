module.exports = function(application) {

    application.get('/sac', function(req, res) {
        application.app.controllers.sac.sac(application, req, res);
    });

    application.get('/atendimento', function(req, res) {
        application.app.controllers.sac.atendimento(application, req, res);
    });
}