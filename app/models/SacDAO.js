function SacDAO(connection){
    this._connection = connection;
}

SacDAO.prototype.getSac = function(callback){
    this._connection.query('SELECT * FROM atendimento ORDER BY data_criacao DESC', callback);
}

SacDAO.prototype.getAtendimento = function(id_atendimento, callback){
    console.log(id_atendimento.id_atendimento);
    this._connection.query('SELECT * FROM atendimento WHERE id_atendimento = ' + id_atendimento.id_atendimento, callback);
}

SacDAO.prototype.salvarAtendimento = function(atendimento, callback){
    this._connection.query('INSERT INTO atendimento SET ?', atendimento, callback);
}

SacDAO.prototype.get10UltimasSac = function(callback){
    this._connection.query('SELECT * FROM atendimento ORDER BY data_criacao DESC LIMIT 10', callback);
}

module.exports = function(){
    return SacDAO;
}