// Requisições padrão commonjs
var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');

// Iniciando o express 
var app = express(); 

/*
*  Template engine
*  Templates
*/
app.set('view engine', 'ejs');
app.set('views', './app/views');

/*
*  Arquivos estaticos
*  Encode para envio dos dados
*  Validacao do form
*/
app.use(express.static('./app/public')); 
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressValidator()); 

/* 
*  Autoload scripts
*  Rotas
*  Conexão com o banco de dados
*  Modelos
*  Controles
*  Run app
*/
consign()
    .include('app/routes')
    .then('config/dbConnection.js')
    .then('app/models')
    .then('app/controllers')
    .into(app);

module.exports = app;