var mysql = require('mysql');

// Conexão MYSQL
var connMySQL = function(){
    return connection = mysql.createConnection({
        host : 'localhost',
		user : 'root',
		password : '123456',
		database : 'sac'
    });
}

module.exports = function(){
    return connMySQL;
}